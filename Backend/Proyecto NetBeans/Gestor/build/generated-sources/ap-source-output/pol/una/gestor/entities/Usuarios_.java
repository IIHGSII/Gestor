package pol.una.gestor.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.gestor.entities.Roles;
import pol.una.gestor.entities.Tareas;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2018-07-31T13:49:04")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, Roles> idrol;
    public static volatile SingularAttribute<Usuarios, BigInteger> telefono;
    public static volatile SingularAttribute<Usuarios, Serializable> nombre;
    public static volatile SingularAttribute<Usuarios, Serializable> email;
    public static volatile SingularAttribute<Usuarios, Serializable> contraseña;
    public static volatile SingularAttribute<Usuarios, Integer> idusuario;
    public static volatile ListAttribute<Usuarios, Tareas> tareasList;

}