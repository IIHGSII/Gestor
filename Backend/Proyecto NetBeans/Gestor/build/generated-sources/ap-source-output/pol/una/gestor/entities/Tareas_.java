package pol.una.gestor.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.gestor.entities.Usuarios;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2018-07-31T13:49:04")
@StaticMetamodel(Tareas.class)
public class Tareas_ { 

    public static volatile SingularAttribute<Tareas, String> descripcion;
    public static volatile SingularAttribute<Tareas, Date> fechainicio;
    public static volatile SingularAttribute<Tareas, String> estado;
    public static volatile SingularAttribute<Tareas, Integer> idtarea;
    public static volatile SingularAttribute<Tareas, Date> fechafin;
    public static volatile SingularAttribute<Tareas, Usuarios> idusuario;

}